using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_Management
{
    class Program
    {
        public class Person
        {
            public int id;
            public string Name, Address, Phone;

        }
        public class Student :Person
        {
            int Class;
            char Grade;
            float Fees, Marks;
            public void info()
            {
                Console.Write("Enter your name: ");
                Name = Convert.ToString(Console.ReadLine());

                Console.Write("Enter you Id :");
                id = Convert.ToInt32(Console.ReadLine());
                
                Console.Write("Enter your Class :");
                Class = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter your Marks :");
                Marks = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Welcome to My School {0}.Your Student Id is : {1}. You are in {2} Standared. Your Marks are {3}.", Name,id,Class,Marks);
                Console.Write("Thank you for your registration ");
                Console.ReadKey();
                
            }

        }
        public class Staff:Person
        {
            public string Designation;
            public double Salary;
        }
        public class Teaching : Staff
        {
            string Qualification, Saubject;

        }
        public class NonTeaching : Staff
        {
            string Dname;
            int MgrId;
        }
        static void Main(string[] args)
        {
            // Create object for the class
            Student s = new Student(); 
            s.info();
            Console.ReadKey();
        }
    }
}
